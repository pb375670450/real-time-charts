const fs = require('fs');
const redis = require('redis');
const request = require('request');
const cheerio = require('cheerio');

const config = require('../config');
const redisShare = redis.createClient(config.redis.port, config.redis.host);

// redisShare.auth(process.env.REDIS_LABS_PW, (err) => {
//   if (err) {
//     console.log(err)
//   };
// });
redisShare.on('connect', (err) => {
  console.log('Connected to Redis');
});

//["https://tradingeconomics.com",,"/gdp-growth"]
//["https://tradingeconomics.com/china/inflation-cpi"]
const Urls = [
  "https://tradingeconomics.com/country-list/gdp-per-capita",
  "https://tradingeconomics.com/country-list/gdp-growth-rate",
  "https://tradingeconomics.com/country-list/gdp-annual-growth-rate",
  "https://tradingeconomics.com/country-list/interest-rate",
  "https://tradingeconomics.com/country-list/inflation-rate",
  "https://tradingeconomics.com/country-list/unemployment-rate",
  "https://tradingeconomics.com/country-list/government-debt-to-gdp",
  "https://tradingeconomics.com/country-list/gdp",
  "https://tradingeconomics.com/country-list/gdp-per-capita",
  "https://tradingeconomics.com/country-list/population",
  "https://tradingeconomics.com/country-list/productivity",
  "https://tradingeconomics.com/country-list/balance-of-trade",
  "https://tradingeconomics.com/country-list/current-account-to-gdp",
  "https://tradingeconomics.com/country-list/crude-oil-production",
  "https://tradingeconomics.com/country-list/foreign-exchange-reserves",
  "https://tradingeconomics.com/country-list/rating",
  "https://tradingeconomics.com/country-list/corporate-tax-rate",
  "https://tradingeconomics.com/country-list/personal-income-tax-rate",
  "https://tradingeconomics.com/country-list/retirement-age-men",
  "https://tradingeconomics.com/country-list/retirement-age-women",
  "https://tradingeconomics.com/forecast/population",
  "https://tradingeconomics.com/forecast/gdp-per-capita"
];

const reduceRows = (elTXT, keyArr) => {
  return formatText(elTXT).reduce((oo, itm, k) => {
    oo[keyArr[k]] = itm;
    return oo;
  }, {});
}

const formatText = (txt) => txt.trim().split(/\s+/g);

const createTable = (thead) => {
  return {tableHead: formatText(thead), rows: []}
}

function scrapeIndicators(country, cb, keyy) {

  let url = 'https://tradingeconomics.com/' + country.toLowerCase() + '/indicators';
  request(url, function (error, response, html) {
    if (error) {
      console.log(error)
    }
    let tables = [];
    let $ = cheerio.load(html);

    $('div[role="main"]').find('table').each(function (i, elem) {
      let table = [];
      $(elem).children().each((c, ee) => {

        if (ee.tagName === "thead") {
          table.push(createTable($(ee).text()));
        }

        if (ee.tagName === "tr") {
          let keyMap = table[table.length - 1].tableHead;
          let newRow = {};
          $(ee).find('td').each((ii, el) => {
            if (keyMap[ii]) {
              newRow[keyMap[ii]] = $(el).text().trim().replace(/\s\s+/g, '');
            }
          });
          table[table.length - 1].rows.push(newRow);

        }

      });
      tables.push(table);

    });
    
    redisShare.set(keyy, JSON.stringify(tables), (err, reply) => {
      if (!err) {
        cb(tables);
        redisShare.expireat(keyy, parseInt((+ new Date) / 1000) + 86400);
      }
    });

  });
};

function getIndicator(country, cb) {
  let keyy = country + '_indicators';
  redisShare.get(keyy, (err, reply) => {
    if (reply) {
      cb(reply);
    } else {
      scrapeIndicators(country, cb, keyy);
    }

  });

}
module.exports = {
  getDataIndicatorsCB: getIndicator
}
